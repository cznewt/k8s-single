#!/bin/bash
FORMULAS=(
"docker"
"etcd"
"git"
"kubernetes"
"linux"
"openssh"
"reclass"
"salt"
)

FORMULA_GIT_BASE="https://github.com/salt-formulas"

mkdir -p ../model/classes/service/

for formula in "${FORMULAS[@]}"; do
	echo "Clone $formula"
	rm -rf "/tmp/$formula/" "./$formula/" "../model/classes/service/${formula}"
	git clone "${FORMULA_GIT_BASE}/salt-formula-${formula}.git" "/tmp/$formula"

	# move formula data
	mv "/tmp/$formula/$formula" ./
	mv "/tmp/$formula/metadata/service/" ../model/classes/service/${formula}/
done
